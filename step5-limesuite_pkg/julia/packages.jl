using Pkg

Pkg.add(PackageSpec(url="https://github.com/hassiweb/LimeSuite.jl", rev="master"))
Pkg.add("PyPlot")

# Run `julia packages.jl` to install packages above

