using Pkg

Pkg.add(PackageSpec(url="https://github.com/dressel/RTLSDR.jl", rev="master"))
Pkg.add("PyPlot")

# Run `julia packages.jl` to install packages above

